<?php


class Reservation {
    /**
     * @var
     */
    private $reservationNo;
    /**
     * @var
     */
    private $date;

    /**
     * @var array
     */
    private $seats;


    private $customer;

    public function __construct($reservationNo,$date){
        $this->setReservationNo($reservationNo);
        $this->setDate($date);
    }

    public function addSeats(Seat $seat){

        $this->seats[]=$seat;
        //de customer is vastgehangen aan de reservatie; hier wordt de reservatie
        // vastgehangen aan de seat en kunnen we zo via flight achterhalen wie de seat heeft geboekt
        $seat->setReservation($this);

        
    }
    public function getSeats()
    {
        return $this->seats;
    }

    /**
     * @return mixed
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param mixed $customer
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;
    }




    public function setReservationNo($reservationNo)
    {
        $this->reservationNo = $reservationNo;
    }

    /**
     * @return mixed
     */
    public function getReservationNo()
    {
        return $this->reservationNo;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }



    /**
     * @return array
     */




} 