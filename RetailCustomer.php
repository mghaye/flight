<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 28/02/14
 * Time: 22:10
 */

class RetailCustomer extends Customer {
    private $creditCardType;
    private $creditCardNo;

    public function __construct($customerId,
                                $lastName,
                                $firstName,
                                $street,
                                $city,
                                $state,
                                $zipCode,
                                $phone,
                                $eMail,
                                $passWord,
                                $creditCardType,
                                $creditCardNo){
        parent::__construct($customerId,
            $lastName,
            $firstName,
            $street,
            $city,
            $state,
            $zipCode,
            $phone,
            $eMail,
            $passWord);
        $this->setCreditCardType($creditCardType);
        $this->setCreditCardNo($creditCardNo);
    }
    public function getHtmlWeergave(){
        $html=parent::getHtmlWeergave();
        $html.='<div class="label-primary">';
        $html.='(Creditcard type :'.$this->getCreditCardType().', Creditcard no : '.$this->getCreditCardNo().')';
        $html.='</div>';
        return $html;

    }

    /**
     * @param mixed $creditCardType
     */
    public function setCreditCardType($creditCardType)
    {
        $this->creditCardType = $creditCardType;
    }

    /**
     * @return mixed
     */
    public function getCreditCardType()
    {
        return $this->creditCardType;
    }

    /**
     * @param mixed $creditCardNo
     */
    public function setCreditCardNo($creditCardNo)
    {
        $this->creditCardNo = $creditCardNo;
    }

    /**
     * @return mixed
     */
    public function getCreditCardNo()
    {
        return $this->creditCardNo;
    }




}