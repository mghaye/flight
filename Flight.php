<?php


class Flight {
    private $flightId;
    private $date;
    private $origin;
    private $destination;
    private $departureTime;
    private $arrivalTime;
    private $seatingCapacity;
    /**
     * @var array
     */
    private $seats;


    public function __construct($flightId,
                                $date,
                                $origin,
                                $destination,
                                $departureTime,
                                $arrivalTime,
                                $seatingCapacity){
        $this->setFlightId($flightId);
        $this->setDate($date);
        $this->setOrigin($origin);
        $this->setDestination($destination);
        $this->setDepartureTime($departureTime);
        $this->setArrivalTime($arrivalTime);
        $this->setSeatingCapacity($seatingCapacity);
    }
    public function addSeats(Seat $seat){
        $this->seats[]=$seat;
    }

    /**
     * @return array
     */
    public function getSeats()
    {
        return $this->seats;
    }
    public function getHtmlWeergave(){
        $html='<div class="label-info">';
        $html.='<h3>Flight id</h3>';
        $html.=$this->getFlightId();
        $html.='<h3>Date</h3>';
        $html.=$this->getDate();
        $html.='<h3>Origin</h3>';
        $html.=$this->getOrigin();
        $html.='<h3>Destination</h3>';
        $html.=$this->getDestination();
        $html.='<h3>Departure time</h3>';
        $html.=$this->getDepartureTime();
        $html.='<h3>Arrival time</h3>';
        $html.=$this->getArrivalTime();
        $html.='<h3>Seating capacity</h3>';
        $html.=$this->getSeatingCapacity();

        $html.='<h3>SEATS</h3>';
        $html.='<ol>';

        if (!($this->getSeats()===null)){
            /**
             * @var Seat $seat
             */
            foreach($this->getSeats() as $seat){
                $html.='<li> Seat No : '.$seat->getSeatNo().'<br>';
                $html.='Row No : '.$seat->getRowNo().'<br>';
                $html.='Price : '.$seat->getPrice().'<br>';
                $html.='Status : '.$seat->getStatus().'</li>';
                $html.='(Reservation no :'.$seat->getReservation()->getReservationNo();
                $html.=', Name : '.$seat->getReservation()->getCustomer()->getLastName().')';

            }

        }
        else{
            $html .='No seats booked ';
        }


        $html.='</ol>';
        $html.='</div>';
        return $html;
    }


    /**
     * @param mixed $flightId
     */
    public function setFlightId($flightId)
    {
        $this->flightId = $flightId;
    }

    /**
     * @return mixed
     */
    public function getFlightId()
    {
        return $this->flightId;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $origin
     */
    public function setOrigin($origin)
    {
        $this->origin = $origin;
    }

    /**
     * @return mixed
     */
    public function getOrigin()
    {
        return $this->origin;
    }

    /**
     * @param mixed $destination
     */
    public function setDestination($destination)
    {
        $this->destination = $destination;
    }

    /**
     * @return mixed
     */
    public function getDestination()
    {
        return $this->destination;
    }

    /**
     * @param mixed $departureTime
     */
    public function setDepartureTime($departureTime)
    {
        $this->departureTime = $departureTime;
    }

    /**
     * @return mixed
     */
    public function getDepartureTime()
    {
        return $this->departureTime;
    }

    /**
     * @param mixed $arrivalTime
     */
    public function setArrivalTime($arrivalTime)
    {
        $this->arrivalTime = $arrivalTime;
    }

    /**
     * @return mixed
     */
    public function getArrivalTime()
    {
        return $this->arrivalTime;
    }

    /**
     * @param mixed $seatingCapacity
     */
    public function setSeatingCapacity($seatingCapacity)
    {
        $this->seatingCapacity = $seatingCapacity;
    }

    /**
     * @return mixed
     */
    public function getSeatingCapacity()
    {
        return $this->seatingCapacity;
    }


} 