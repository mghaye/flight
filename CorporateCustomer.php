<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 1/03/14
 * Time: 16:22
 */

class CorporateCustomer extends Customer {
    private $companyName;
    private $frequentFlyerPts;
    private $billingAccountNo;


    public function __construct($customerId,
                                $lastName,
                                $firstName,
                                $street,
                                $city,
                                $state,
                                $zipCode,
                                $phone,
                                $eMail,
                                $passWord,
                                $companyName,
                                $frequentFlyerPts,
                                $billingAccountNo){
        parent::__construct($customerId,
            $lastName,
            $firstName,
            $street,
            $city,
            $state,
            $zipCode,
            $phone,
            $eMail,
            $passWord);
        $this->setCompanyName($companyName);
        $this->setFrequentFlyerPts($frequentFlyerPts);
        $this->setBillingAccountNo($billingAccountNo);
    }

    public function getHtmlWeergave(){
        $html=parent::getHtmlWeergave();
        $html.='<div class="label-primary">';
        $html.= '(Companyname : '.$this->getCompanyName();
        $html.=', frequent flyerpoints : ' .$this->getFrequentFlyerPts();
        $html.=', billing account no : ' .$this->getBillingAccountNo().')<br>';
        $html.='</div>';
        return $html;

    }



    /**
     * @param mixed $billingAccountNo
     */
    public function setBillingAccountNo($billingAccountNo)
    {
        $this->billingAccountNo = $billingAccountNo;
    }

    /**
     * @return mixed
     */
    public function getBillingAccountNo()
    {
        return $this->billingAccountNo;
    }

    /**
     * @param mixed $companyName
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;
    }

    /**
     * @return mixed
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * @param mixed $frequentFlyerPts
     */
    public function setFrequentFlyerPts($frequentFlyerPts)
    {
        $this->frequentFlyerPts = $frequentFlyerPts;
    }

    /**
     * @return mixed
     */
    public function getFrequentFlyerPts()
    {
        return $this->frequentFlyerPts;
    }



} 