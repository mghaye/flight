<?php
class Seat {
    private $rowNo;
    private $seatNo;
    private $price;
    private $status;
    private $reservation;


    public function __construct($rowNo,$seatNo,$price,$status){
        $this->setRowNo($rowNo);
        $this->setSeatNo($seatNo);
        $this->setPrice($price);
        $this->setStatus($status);

    }

    /**
     * @param mixed $reservation
     */
    public function setReservation($reservation)
    {
        $this->reservation = $reservation;
    }

    /**
     * @return mixed
     */
    public function getReservation()
    {
        return $this->reservation;
    }


    /**
     * @param mixed $rowNo
     */
    public function setRowNo($rowNo)
    {
        $this->rowNo = $rowNo;
    }

    /**
     * @return mixed
     */
    public function getRowNo()
    {
        return $this->rowNo;
    }

    /**
     * @param mixed $seatNo
     */
    public function setSeatNo($seatNo)
    {
        $this->seatNo = $seatNo;
    }

    /**
     * @return mixed
     */
    public function getSeatNo()
    {
        return $this->seatNo;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }



} 