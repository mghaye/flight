<?php

 abstract class Customer implements ICustomer {
    private $customerId;
    private $lastName;
    private $firstName;
    private $street;
    private $city;
    private $state;
    private $zipCode;
    private $phone;
    private $eMail;
    private $passWord;
    /**
     * @var array
     */
    private $reservations;


    public function __construct($customerId,
                                $lastName,
                                $firstName,
                                $street,
                                $city,
                                $state,
                                $zipCode,
                                $phone,
                                $eMail,
                                $passWord){
        $this->setCustomerId($customerId);
        $this->setLastName($lastName);
        $this->setFirstName($firstName);
        $this->setStreet($street);
        $this->setCity($city);
        $this->setState($state);
        $this->setZipCode($zipCode);
        $this->setPhone($phone);
        $this->setEMail($eMail);
        $this->setPassWord($passWord);

    }
    public function addReservation(Reservation $reservation){
        //reservatie wordt toegevoegd aan onze array
        $this->reservations[]=$reservation;
        //de customer wordt vastgehangen aan de reservatie; daarna wordt de reservatie
        //(bij addSeat) vastgehangen aan de seat en kunnen we zo via flight achterhalen wie de seat heeft geboekt
        $reservation->setCustomer($this);
    }
    /**
     * @return array
     */
    public function getReservations()
    {
        return $this->reservations;
    }
    public function getHtmlWeergave(){
        $html = '<div class="label-info">';

        $html .= '<h2>'.$this->getCustomerId() .' / '. $this->getLastName() . ' ' . $this->getFirstName() . '</h2>';


        $html .= '<h3>Adress</h3>';

        $html .= '<ul>';
        $html .= '<li>' . $this->getStreet() . ' ' . $this->getCity() . '</li>';
        $html .= '<li>' . $this->getZipCode() . ' '. $this->getState() . '</li>';
        $html .= '</ul>';

        $html .= '<h3>Phone</h3>';
        $html .= '<p>' . $this->getPhone() . '</p>';

        $html .= '<h3>E-Mail</h3>';
        $html .= '<p>' . $this->getEMail() . '</p>';

        $html .= '<h3>Reservations</h3>';
        $html .= '<ul>';

        if (!($this->getReservations()===null)){
            /**
             * @var Reservation $reservation
             */
            foreach($this->getReservations() as $reservation) {
                $html .= '<li> Reservation No : ' . $reservation->getReservationNo().'<br>' ;
                $html .='Reservation Date : '.$reservation->getDate().'</li>';
                /**
                 * @var Seat $seat
                 */
                $html.='<ol>';
                //TODO controle inbouwen of seat niet null is (hoeft eigenlijk niet?)
                foreach($reservation->getSeats() as $seat){
                    $html.='<li> Seat No : '.$seat->getSeatNo().'<br>';
                    $html.='Row No : '.$seat->getRowNo().'<br>';
                    $html.='Price : '.$seat->getPrice().'<br>';
                    $html.='Status : '.$seat->getStatus().'</li>';

                }
                $html.='</ol>';
            }
        }
        else{
            $html .='<li>No reservations booked </li>';
        }
        $html .= '</ul>';
        $html .= '</div>';



        return $html;


    }

    /**
     * @param mixed $customerId
     */
    public function setCustomerId($customerId)
    {
        $this->customerId = $customerId;
    }

    /**
     * @return mixed
     */
    public function getCustomerId()
    {
        return $this->customerId;
    }

    /**
     * @param mixed $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $firstName
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $street
     */
    public function setStreet($street)
    {
        $this->street = $street;
    }

    /**
     * @return mixed
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $zipCode
     */
    public function setZipCode($zipCode)
    {
        $this->zipCode = $zipCode;
    }

    /**
     * @return mixed
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $eMail
     */
    public function setEMail($eMail)
    {
        $this->eMail = $eMail;
    }

    /**
     * @return mixed
     */
    public function getEMail()
    {
        return $this->eMail;
    }

    /**
     * @param mixed $passWord
     */
    public function setPassWord($passWord)
    {
        $this->passWord = $passWord;
    }

    /**
     * @return mixed
     */
    public function getPassWord()
    {
        return $this->passWord;
    }




} 