<html>
<head>
    <meta charset="UTF-8">
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/bootstrap-theme.css" rel="stylesheet">
</head>

<body>
<div class="container">
<?php
include 'autoload.php';
spl_autoload_register("autoload");

$CorporateCustomer1=new CorporateCustomer('1c','Ulenaers','Hilde','Heesakkerstraat',
    'Overpelt','Belgium',3900,'011802880','hilde.ulenaers@telenet.be','lotje19','SecondHandShop',112,2304 );
$CorporateCustomer2=new CorporateCustomer('2c','Plop','Kabouter','Zwamstraat','Funcity','Fabeltjesland',
    8000,'044444444444','Plop@skynet.be','Plopperdeplop','CleanDish',230,2222644545);
$RetailCustomer1=new RetailCustomer('1r','Ghaye','Mark','Luikersteenweg','Tongeren','Belgium',
    3700,'012234117','mark.ghaye@telenet.be','sousa19','bancontact','12545464646464');


$reservatie1=new Reservation(1202,'25/05/2014');
$reservatie2=new Reservation(1204,'12/06/2014');
$reservatie3=new Reservation(1507,'28/05/2014');

$seat1=new Seat('AG',7,'€345','Economy');
$seat2=new Seat('GT',1,'€410','Business');
$seat3=new Seat('HN',5,'€550','First Class');
$seat4=new Seat('XK',1,'€380','Economy');

$reservatie1->addSeats($seat2);
$reservatie2->addSeats($seat1);
$reservatie2->addSeats($seat3);
$reservatie3->addSeats($seat4);

$CorporateCustomer1->addReservation($reservatie1);
$CorporateCustomer1->addReservation($reservatie2);
$RetailCustomer1->addReservation($reservatie3);

$flight1=new Flight(9546613,'18/06/2014','Liege','Kiev','14:30','21:45',300);
$flight1->addSeats($seat1);
$flight1->addSeats($seat2);
$flight1->addSeats($seat3);
$flight2=new Flight(8542223,'20/07/2014','Brussels','New York','15:00','08:00',210);
$flight2->addSeats($seat4);
$flight3=new Flight(7542315,'24/11/2014','Schiphol','Peking','8:00','0:45',340);



/*echo $CorporateCustomer1->getHtmlWeergave();
echo $RetailCustomer1->getHtmlWeergave();
echo $flight1->getHtml();*/
?>
    <br>
    <div class="form-control">

        <form action="" method="post">
            <select name="customers">
                <option value="" selected >Please select a customer</option>
                <option value="1c">CorporateCustomer1</option>
                <option value="2c">CorporateCustomer2</option>
                <option value="1r">RetailCustomer1</option>
            </select>

            <input type="submit" value="Choose customer" name="customer">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <select name="flights" >
                <option value="" selected >Please select a flight</option>
                <option value=9546613>Flight1</option>
                <option value=8542223>Flight2</option>
                <option value=7542315>Flight3</option>

            </select>
            <input type="submit" value="Choose flight" name="flight">

        </form>

    </div>

    <?php


    if (isset($_POST['customers'])==true){
        $customerId=$_POST['customers'];

        if ($CorporateCustomer1->getCustomerId()==$customerId){
            echo $CorporateCustomer1->getHtmlWeergave();
        }
        elseif($CorporateCustomer2->getCustomerId()===$customerId){
            echo $CorporateCustomer2->getHtmlWeergave();
        }
        elseif($RetailCustomer1->getCustomerId()==$customerId){
            echo $RetailCustomer1->getHtmlWeergave();
        }


    }
    if (isset($_POST['flights'])==true){
        $flightId=$_POST['flights'];

        if ($flight1->getFlightId()==$flightId){
            echo $flight1->getHtmlWeergave();
        }
        elseif($flight2->getFlightId()==$flightId){
            echo $flight2->getHtmlWeergave();
        }
        elseif($flight3->getFlightId()==$flightId){
            echo $flight3->getHtmlWeergave();
        }
    }

    ?>
</div>
<script src="js/bootstrap.js"></script>
</body>

</html>
